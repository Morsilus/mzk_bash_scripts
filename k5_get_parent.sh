#!/bin/bash

for pid in $@; do
	parent=`curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$pid%22&wt=json" | jq -r '.response.docs[0].parent_pid[0]'`
	echo $pid,$parent
done
