#!/bin/bash

tmp="/tmp/sdnnt_granularity.json.tmp"

for pid in $@; do
	curl -s -H 'accept: application/json' "https://sdnnt.nkp.cz/sdnnt/api/v1.0/lists/info/$pid" > $tmp
	licenses=`cat $tmp | jq -r --arg pid "$pid" '.items[].granularity? // empty | .[] | select(.pid == $pid) | .license' 2> /dev/null | tr -d '|' 2> /dev/null`
	states=`cat $tmp | jq -r --arg pid "$pid" '.items[].granularity? // empty | .[] | select(.pid == $pid) | .states[-1]' 2> /dev/null | tr -d '|' 2> /dev/null`
	echo $pid,$licenses,$states
	rm $tmp
done
