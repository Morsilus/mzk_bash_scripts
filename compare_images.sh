#!/bin/bash

if [ $# -ne 2 ]; then
	exit 1
fi

feh -F $1 $2

read -n 1 input

if [ $input == 'y' ]; then
	echo $1,$2 >> same_images.csv
else
	echo $1,$2,0 >> different_images.csv
fi
