#!/bin/bash

for pid in $@; do
	code=`curl -I -s -o /dev/null -w "%{http_code}" $K7_HOST/api/admin/v7.0/items/$pid/streams/RELS-EXT -H 'Accept: application/json' -H "Authorization: Bearer $K7_TOKEN"`
	if [[ $code -eq 200 ]]; then
		echo $pid,true
	else
		echo $pid,false
	fi
done
