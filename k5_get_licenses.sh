#!/bin/bash

for pid in $@; do
	licenses=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '."dnnt-labels"[]' 2> /dev/null`
	echo $pid,$licenses
done
