#!/bin/bash

if [ $# -ne 1 ]; then
	exit 1
fi

for doc in `cat $1`; do
	mysql -u robert -D Pages -e "SELECT p.PageUUID, p.Index, p.Type, p.Number, p.Placement FROM Page p WHERE ParentUUID = '$doc'" > pages.csv
	tail -n +2 pages.csv | awk '{print "\""$1F"\",\""$2F"\",\""$3F"\",\""$4F"\""}' > $doc-pages.csv
done
rm pages.csv
