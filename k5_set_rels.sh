#!/bin/bash

for pid in $@; do
	java -jar ~/scripts/WriteStream.jar -h $FEDORA_HOST -u $FEDORA_USER -p $FEDORA_PASSWD -d RELS_EXT -i $pid -x $pid\_rels.xml
done
