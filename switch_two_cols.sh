#/bin/bash

awk 'BEGIN {FS=OFS=","} {temp=$1; $1=$2; $2=temp} 1' $1
