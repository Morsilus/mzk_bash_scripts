#!/bin/bash

old="norway"
new="ilnorway"

java -jar ~/scripts/GetStream.jar -h $FEDORA_HOST -u $FEDORA_USER -p $FEDORA_PASSWD -d RELS_EXT -i $1 > $1\_rels.xml
cat $1\_rels.xml | sed "s/donator:$old/donator:$new/" > $1\_rels_new.xml
_diff=$(diff -q $1\_rels.xml $1\_rels_new.xml)
if [[ $_diff != "" ]]; then
	java -jar ~/scripts/WriteStream.jar -h $FEDORA_HOST -u $FEDORA_USER -p $FEDORA_PASSWD -d RELS_EXT -i $1 -x $1\_rels_new.xml
	rm $1\_rels_new.xml
fi
rm $1\_rels.xml
