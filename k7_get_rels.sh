#!/bin/bash

for pid in $@; do
	curl $K7_HOST/api/admin/v7.0/items/$pid/streams/RELS-EXT -H 'Accept: application/json' -H "Authorization: Bearer $K7_TOKEN" > $pid\_rels.xml
done
