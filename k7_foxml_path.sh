#!/bin/bash

# filepath: ##/##/##
dir="/mnt/akubra/objectStore/"
file_prefix="info%3Afedora%2Fuuid%3A"

for pid in $@; do
	hash=`echo -n info:fedora/$pid | md5sum`
	sliced_pid=`echo $pid | cut -d: -f2`
	echo $dir${hash:0:2}/${hash:2:2}/${hash:4:2}/$file_prefix$sliced_pid
done
