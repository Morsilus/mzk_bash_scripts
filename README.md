Zbierka rôzných bash skriptov, jednoduchých wrapperov nad API pointami K5 a K7. Môže slúžiť ako ukážka rôznych API volaní. Niektoré skripty nemusia byť up-to-date s úpravami v K7 API.
