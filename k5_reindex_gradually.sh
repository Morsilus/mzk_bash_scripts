#!/bin/bash

auth="krameriusAdmin:krameriusAdmin"
header="\"Content-Type: application/json\""
get_num_planned="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?state=PLANNED&resultSize=100"
plan_reindex="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?def=reindex"
log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

max_planned=25
wait_time=60
#reindex_type="reindexDoc"
reindex_type="fromKrameriusModel"
#reindex_type="fromKrameriusModelNoCheck"

planned_n=0
for pid in $@; do
	planned=`curl -s -u $auth -H $header $get_num_planned | jq length`
	while [[ $planned -ge $max_planned ]]; do
		sleep $wait_time
		planned=`curl -s -u $auth -H $header $get_num_planned | jq length`
	done

	body="'{\"parameters\":[\"$reindex_type\",\"$pid\",\"$pid\"]}'"
	cmd="curl -su $auth -H $header -X POST $plan_reindex -d $body | sed 's/$/\n/' >> $log_file"
	eval $cmd

	planned_n=`echo "$planned_n+1" | bc`
	echo "$planned_n/$#"
done
