#!/bin/bash

for file in `ls *solr.xml`; do uuid=`echo $file | cut -d_ -f1`; level=`xmllint --format --xpath '//*[local-name()="int"][@name="level"]/text()' $file || echo "-1"`; echo $uuid,$level; done
