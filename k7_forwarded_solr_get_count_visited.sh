#!/bin/bash

for pid in $@; do
	# count=`curl -s "http://localhost:8983/solr/logs/select?indent=true&q.op=OR&q=all_pids%3A%22$pid%22&rows=0&useParams=" | jq '.response.numFound'`
	count=`curl -s "http://localhost:8983/solr/logs/select?indent=true&q.op=OR&q=all_pids%3A%22$pid%22%20AND%20date%3A%5B\"2023-01-01T00%3A00%3A00.000Z\"%20TO%20\"2023-12-31T23%3A59%3A59.999Z\"%5D&rows=0&useParams=" | jq '.response.numFound'`
	echo $pid,$count
done

