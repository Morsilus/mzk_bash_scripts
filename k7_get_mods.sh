#!/bin/bash

for uuid in $@; do
	curl $K7_HOST/api/client/v7.0/items/$uuid/metadata/mods  > $uuid\_mods.xml
done
