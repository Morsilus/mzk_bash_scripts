#!/bin/bash

for pid in $@; do
	curl "$K7_HOST/api/admin/v7.0/collections/$pid" -H 'Accept: application/json' -H "Authorization: Bearer $K7_TOKEN" $pid.coll
done
