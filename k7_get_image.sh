#!/bin/bash

for uuid in $@; do
	curl $K7_HOST/api/admin/v7.0/items/$uuid/image/preview -H 'Accept: application/json' -H "Authorization: Bearer $K7_TOKEN" > $uuid\_image.xml
done
