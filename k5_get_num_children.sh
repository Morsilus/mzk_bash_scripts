#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s $K5_HOST/api/v5.0/item/$pid/children | jq -r '. | length'`
done
