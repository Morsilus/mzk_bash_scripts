#!/bin/bash

if [[ $# -eq 3 ]]; then
	fields=`echo $3 | sed 's/,/%2C/'`
	rows=$2
elif [[ $# -eq 2 ]]; then
	fields="pid"
	rows=$2
elif [[ $# -eq 1 ]]; then
	fields="pid"
	rows=1000
else
	exit 1
fi

query=`echo $1 | jq -s -R -r @uri`
response=`curl -s "$SOLR_HOST/select?q=$query&fl=$fields&rows=$rows&wt=json"`

echo $response | jq -r ".response.docs[] | map(if type == \"array\" then join(\"|\") else . end) | join(\",\")"
