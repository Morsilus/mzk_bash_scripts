#!/bin/bash

for file in $@; do
	if ! [ -f $file ]; then exit 1; fi
	sed -i 's/<?xml version="1.0" encoding="UTF-8"?>//' $file
done
