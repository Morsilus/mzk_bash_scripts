#/bin/bash

tmp="/tmp/foxml_remove_img_streams.tmp"

if [[ $# -ne 1 ]]; then
	exit 1
fi
if ! [[ -f $1 ]]; then
	exit 1
fi

xmlstarlet ed -d '//*[local-name()="datastream"][@ID="IMG_THUMB" or @ID="IMG_PREVIEW" or @ID="IMG_FULL" or @ID="IMG_FULL_ADM" or @ID="IMG_FULL_AMD"]' $1 > $tmp

mv $tmp $1
