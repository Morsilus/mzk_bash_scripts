#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
plan_reindex="$K7_DEV_HOST/api/admin/v7.0/processes"
log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

#reindex_type="OBJECT"
reindex_type="TREE_AND_FOSTER_TREES"

processed_n=0
for pid in $@; do
	body="'{\"defid\":\"new_indexer_index_object\",\"params\":{\"type\":\"$reindex_type\",\"pid\":\"$pid\"}}'"
	cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_reindex\" | sed 's/$/\n/' >> $log_file"
	eval $cmd

	processed_n=`echo "$processed_n+1" | bc`
	echo "$processed_n/$#"
done
