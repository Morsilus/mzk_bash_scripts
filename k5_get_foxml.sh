#!/bin/bash

for uuid in $@; do
	curl -u $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/objects/$uuid/objectXML > $uuid.foxml
done
