#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s $K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22 | jq -r '.response.docs[0].accessibility'`
done
