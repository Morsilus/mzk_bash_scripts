#!/bin/bash

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/children | jq -r '.[].pid' > $pid\_children
done
