#!/bin/bash

if [[ $# -ne 2 ]]; then
	exit 1
fi

echo $2
python -c "from Levenshtein import distance as lev; mean_l = (len(\"$1\") + len(\"$2\")) / 2; print((mean_l - lev(\"$1\", \"$2\")) / mean_l)"
