#!/bin/bash

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS > /tmp/$pid\_mods.xml
	placement=`xmllint --xpath '//*[local-name()="note"]/text()' /tmp/$pid\_mods.xml`
	echo $pid,$placement
	rm /tmp/$pid\_mods.xml
done
