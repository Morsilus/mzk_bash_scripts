#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=date_range_end.year" | jq -r '.response.docs[0]."date_range_end.year"'`
done
