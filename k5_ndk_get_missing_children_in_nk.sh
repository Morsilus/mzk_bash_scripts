#!/bin/bash

nk_host="https://kramerius5.nkp.cz/search"
tmp_mzk="/tmp/ndk_get_missing_children_mzk_`date +%s`"
tmp_nk="/tmp/ndk_get_missing_children_nk_`date +%s`"

for pid in $@; do
	curl -s "$K5_HOST/api/v5.0/item/$pid/children" | jq '.[].details.partNumber' > $tmp_mzk
	curl -s "$nk_host/api/v5.0/item/$pid/children" | jq '.[].details.partNumber' > $tmp_nk
	sort -o $tmp_mzk $tmp_mzk
	sort -o $tmp_nk $tmp_nk
	comm -23 $tmp_mzk $tmp_nk | sed 's/\"//g' | sed 's/\s/,/g'
done

rm $tmp_mzk $tmp_nk
