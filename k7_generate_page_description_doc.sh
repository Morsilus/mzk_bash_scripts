#!/bin/bash

file_prefix="info%3Afedora%2Fuuid%3A"

for pid in $@; do
	data=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.title, .model' 2> /dev/null | paste -sd\|`
	page_number=`echo $data | cut -d\| -f1`
	model=`echo $data | cut -d\| -f2`

	if [[ $model != "page" ]]; then
		continue
	fi

	hash=`echo -n info:fedora/$pid | md5sum`
	sliced_pid=`echo $pid | cut -d: -f2`
	ref="file:`echo ${hash:0:2}/${hash:2:2}/${hash:4:2}/$file_prefix$sliced_pid`"
	echo "{
        \"source\":\"$pid\",
        \"type\":\"description\",
        \"model\":\"model:page\",
        \"dc.title\":\"$page_number\",
        \"ref\":\"$ref\",
		\"date\":\"$(date -u +"%Y-%m-%dT%H:%M:%S.%3NZ")\",
        \"pid\":\"description|$pid\"
	}" | jq > $sliced_pid.json
done
