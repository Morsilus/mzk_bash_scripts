#!/bin/bash

for pid in $@; do
	length=`curl -s $K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22 | jq '.response.docs[0]' | jq length`
	if [[ $length -lt 5 ]]; then
		echo $pid,False
	else
		echo $pid,True
	fi
done
