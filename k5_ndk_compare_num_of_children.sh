#!/bin/bash

nk_host="https://kramerius5.nkp.cz/search"

for pid in $@; do
	mzk=`curl -s "$K5_HOST/api/v5.0/item/$pid/children" | jq length`
	nk=`curl -s "$nk_host/api/v5.0/item/$pid/children" | jq length`
	if [[ $mzk -ne $nk ]]; then
		echo $pid
	fi
done
