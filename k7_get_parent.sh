#!/bin/bash

for pid in $@; do
	parent=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=own_parent.pid" | jq -r '.response.docs[0]."own_parent.pid"'`
	echo $pid,$parent
done
