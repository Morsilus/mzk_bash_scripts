import sys
import requests
import urllib
import json

TOKEN_CALL = "{KEYCLOAK_HOST}/realms/kramerius/protocol/openid-connect/token"
TOKEN_KEY = "access_token"
CLIENT_ID = "krameriusClient"

if len(sys.argv) != 5:
    exit(1)

keycloak_host = sys.argv[1]
keycloak_secret = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

body = {
    'client_id': CLIENT_ID,
    'client_secret': keycloak_secret,
    'username': username,
    'password': password,
    'grant_type': 'password'
}

r = requests.post(TOKEN_CALL.format(KEYCLOAK_HOST = keycloak_host), data=body)

if r.ok:
    dictData = json.loads(r.text)
    token = dictData['access_token']
    print(token)
else:
    exit(1)
