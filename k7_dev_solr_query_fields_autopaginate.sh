#!/bin/bash

BATCH_SIZE=10000

preserve_fields=0
fields="pid"

if [[ $# -eq 3 ]]; then
	preserve_fields=$3
	fields=`echo $2 | sed 's/,/%2C/g'`
elif [[ $# -eq 2 ]]; then
	fields=`echo $2 | sed 's/,/%2C/g'`
elif [[ $# -ne 1 ]]; then
	exit 1
fi

query=`echo $1 | jq -s -R -r @uri`

batch_count=$((`curl -s "$K7_DEV_HOST/api/client/v7.0/search?q=$query&fl=$fields&rows=0" | jq '.response.numFound'` / BATCH_SIZE))

for batch in `seq 0 $batch_count`; do
	offset=$((BATCH_SIZE * batch))

	response=`curl -s "$K7_DEV_HOST/api/client/v7.0/search?q=$query&fl=$fields&rows=$BATCH_SIZE&start=$offset"`

	if [[ $preserve_fields -eq 1 ]]; then
		max_index=$((`echo "$response" | jq '.response.docs | length'` - 1))
		fields_list="`echo $fields | sed 's/\%2C/ /g'`"

		for i in `seq 0 $max_index`; do
			document=$(echo "$response" | jq -r ".response.docs[${i}]")
			echo $(for field in $fields_list; do
				echo $document | jq -r '."'"$field"'" | if type == "array" then join(",") else . end'
			done | paste -sd\|)
		done
	else	
		echo $response | jq -r ".response.docs[] | map(if type == \"array\" then join(\"|\") else . end) | join(\",\")"
	fi
done

