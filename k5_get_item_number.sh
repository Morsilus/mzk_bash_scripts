#!/bin/bash

tmp_ext="_mods.xml.tmp"

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS > $pid$tmp_ext
	number=`xmllint --xpath '//*[local-name()="partNumber"]/text()' $pid$tmp_ext 2> /dev/null`
	if [[ $number == "" ]]; then
		number=`xmllint --xpath '//*[local-name()="number"]/text()' $pid$tmp_ext 2> /dev/null`
	fi
	echo $number
	rm $pid$tmp_ext
done
