#!/bin/bash

tmp_solr="/tmp/is_from_ndk_`date +%s`_solr.tmp"

for pid in $@; do
	curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$pid%22&wt=json" > $tmp_solr
	model=`cat $tmp_solr | jq -r '.response.docs[]."fedora.model"'`
	level=`cat $tmp_solr | jq -r '.response.docs[].level'`
	page=""
	if [[ $model == "periodical" ]]; then
		echo $pid,periodical
		continue
	elif [[ $level -eq 0 ]]; then
		last_pid=`curl -s "$K5_HOST/api/v5.0/search?q=root_pid:%22$pid%22%20AND%20fedora.model:page&rows=1&wt=json" | jq -r '.response.docs[].PID'`
	else
		last_pid=$pid
		model=""
		while [[ $model != "page" ]]; do
			last_pid=`curl -s "$K5_HOST/api/v5.0/search?q=parent_pid:%22$last_pid%22%20AND%20-PID:%22$last_pid%22&rows=1&wt=json" | jq -r '.response.docs[].PID'`
			if [[ $last_pid == "" ]]; then
				break
			fi
			model=`curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$last_pid%22&wt=json" | jq -r '.response.docs[]."fedora.model"'`
		done
	fi

	if [[ $last_pid != "" ]]; then
		if curl -su $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/get/$last_pid/RELS-EXT | grep -q "http://imageserver.mzk.cz/NDK/"; then
			echo $pid,True
		else
			echo $pid,False
		fi
	else
			echo $pid,error
	fi
done

rm $tmp_solr
