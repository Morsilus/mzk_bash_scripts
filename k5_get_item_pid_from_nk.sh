#!/bin/bash

nk_host="https://kramerius5.nkp.cz/search"

curl -s $nk_host/api/v5.0/item/$1/children | jq ".[] | select(.details.partNumber == \"$2\") | .pid" | sed 's/\"//g'
