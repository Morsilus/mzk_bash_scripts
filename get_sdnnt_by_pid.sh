#!/bin/bash

tmp="/tmp/get_sdnnt_by_pid.log"

header="accept: application/json"

for pid in $@; do
	curl -s -H "$header" "https://sdnnt.nkp.cz/sdnnt/api/v1.0/lists/info/$pid" > $tmp
	num_found=`cat $tmp | jq -r '.numFound'`
	if [[ $num_found -eq 1 ]]; then 
		state=`cat $tmp | jq -r '.items[0].state'`
		license=`cat $tmp | jq -r '.items[0].license'`
		title=`cat $tmp | jq '.items[].title'`
		echo $pid,$state,$license,$title
	else
		echo $pid,not_found
	fi
done
