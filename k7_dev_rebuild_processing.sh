#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
plan_process="$K7_DEV_HOST/api/admin/v7.0/processes"
log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

processed_n=0
for pid in $@; do
	body="'{\"defid\":\"processing_rebuild_for_object\",\"params\":{\"pid\":\"$pid\"}}'"
	cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_process\" | sed 's/$/\n/' >> $log_file"
	eval $cmd
	
	processed_n=`echo "$processed_n+1" | bc`
	echo "$processed_n/$#"
done
