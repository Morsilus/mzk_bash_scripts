#!/bin/bash

for pid in $@; do
	parent=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=own_parent.pid" | jq -r '.response.docs[0]."own_parent.pid"'`
	year=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$parent%22&fl=date_range_start.year" | jq -r '.response.docs[0]."date_range_start.year"'`
	echo $pid,$year
done
