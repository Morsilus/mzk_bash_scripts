#!/bin/bash

for file in $@; do
	#echo "{\"params\":{\"pidlist\":$(cat $file | jq -R | jq -s),\"type\":\"OBJECT\"},\"defid\":\"new_indexer_index_object\"}" | jq > /tmp/pidlist.json
	echo "{\"params\":{\"pidlist\":$(cat $file | jq -R | jq -s),\"type\":\"TREE_AND_FOSTER_TREES\", \"ignoreInconsistentObjects\":true},\"defid\":\"new_indexer_index_object\"}" | jq > /tmp/pidlist.json
	curl -H "Content-Type: application/json" -H "Authorization: Bearer $K7_TOKEN" "$K7_HOST/api/admin/v7.0/processes" --data @/tmp/pidlist.json -X POST
	#rm /tmp/pidlist.json
done
