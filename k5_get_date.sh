#!/bin/bash

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.details.date'
done
