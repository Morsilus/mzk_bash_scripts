#!/bin/bash

get_pids=".[{i}].model"
tmp_volumes="/tmp/find_items_in_wrong_volume_`date +%s`_volumes.tmp"
tmp_all_numbers="/tmp/find_missing_items_`date +%s`_all.tmp"

for pid in $@; do
	model=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.model'`
	if [[ $model != "periodical" ]]; then
		break
	fi

	for volume_pid in `curl -s $K5_HOST/api/v5.0/item/$pid/children | jq -r '.[].pid'`; do
		model=`curl -s $K5_HOST/api/v5.0/item/$volume_pid | jq -r '.model'`
		if [[ $model != "periodicalvolume" ]]; then
			echo "$pid,$volume_pid,not_volume"
			break
		fi

		volume_year_str=`curl -s $K5_HOST/api/v5.0/item/$volume_pid | jq -r '.details.year'`
		if echo $volume_year_str | grep -q "[0-9]\{4\}\s*-\s*[0-9]\{4\}"; then
			volume_year=`echo $volume_year_str| sed 's/.*\([0-9]\{4\}\)\s*-.*/\1/'`
			volume_year_2=`echo $volume_year_str| sed 's/.*-.*\([0-9]\{4\}\).*/\1/'`
		else
			volume_year=`echo $volume_year_str | sed 's/.*\([0-9]\{4\}\).*/\1/'`
			volume_year_2=$volume_year
		fi

		if echo $volume_year | grep -qv "[0-9]\{4\}"; then
			echo $pid,$volume_year,err
			break
		fi

		last_year=`echo "$volume_year-1" | bc`
		for item_pid in `curl -s $K5_HOST/api/v5.0/item/$volume_pid/children | jq -r '.[].pid'`; do
			model=`curl -s $K5_HOST/api/v5.0/item/$item_pid | jq -r '.model'`
			if [[ $model == "page" ]]; then
				echo "$pid,$volume_pid,pages_in_volume"
				break
			fi

			item_json=`curl -s $K5_HOST/api/v5.0/item/$item_pid`
			#echo $item_json | jq -r
			item_year=`echo $item_json | jq -r '.details.date' | sed 's/.*\([0-9]\{4\}\).*/\1/'`
			item_number=`echo $item_json | jq -r '.details.partNumber'`

			if [[ $item_year == "\"\"" ]]; then
				break
			fi
			if [[ $item_year -eq $last_year ]]; then
				if echo $item_number | grep -q "^[0-9]*$"; then
					if [[ $item_number -eq 1 ]]; then
						break
					fi
				fi
			fi
			if [[ $volume_year -gt $item_year || $item_year -gt $volume_year_2 ]]; then
				echo $pid,$volume_pid,$item_pid,$volume_year,$item_year
			fi
		done
	done
done
