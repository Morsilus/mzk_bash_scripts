#!/bin/bash

for dir in $@; do
	if ! [ -d $dir ]; then
		mkdir $dir
	fi
	sshfs -o allow_other randiak@editor:$dir $dir
done
