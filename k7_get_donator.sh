#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=donator" | jq -r '.response.docs[0].donator[]' 2>/dev/null`
done
