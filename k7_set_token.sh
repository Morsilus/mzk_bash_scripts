#!/bin/bash

export K7_TOKEN=$(curl -X POST -H "Content-Type: application/json" -d "username=$K7_USER&password=$K7_PASSWD" $K7_HOST/api/auth/token | jq '.access_token' | sed 's/\"//g')
