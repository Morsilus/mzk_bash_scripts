#!/bin/bash

if [ $# -ne 1 ]; then
	exit 1
fi
if ! [ -d $1 ]; then
	exit 1
fi

for scan in `find $1 -type f -regex ".*.jp2$"`; do
	info=`jpylyzer $scan`
	if echo $info | grep -q "\(<width>1</widht>\|<height>1</height>\)"; then
		echo "$scan is corrupted"
	fi
done
