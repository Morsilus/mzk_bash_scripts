#!/bin/bash

rows=100
if [[ $# -eq 1 ]]; then
	rows=$1
elif [[ $# -ne 0 ]]; then
	exit 1
fi

curl -H "Authorization: Bearer $K7_TOKEN" "$K7_HOST/api/admin/v7.0/sdnnt/sync?rows=$rows"
