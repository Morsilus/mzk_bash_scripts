#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_num_running="$K7_HOST/api/admin/v7.0/processes/batches?state=RUNNING&resultSize=1"
get_num_planned="$K7_HOST/api/admin/v7.0/processes/batches?state=PLANNED&resultSize=1"

echo Number of running processes: `curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
echo Number of planned processes: `curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
