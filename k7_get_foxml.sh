#!/bin/bash

for uuid in $@; do
	curl $K7_HOST/api/admin/v7.0/items/$uuid/foxml -H 'Accept: application/xml' -H "Authorization: Bearer $K7_TOKEN" > $uuid.foxml
done
