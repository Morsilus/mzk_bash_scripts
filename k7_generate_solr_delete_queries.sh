#!/bin/bash

for pid in $@; do
	echo "source:\"$pid\" OR"
done
for pid in $@; do
	echo "targetPid:\"$pid\" OR"
done

echo
for pid in $@; do
	echo "{'delete':{'query':'source:\"$pid\"'}}"
done
for pid in $@; do
	echo "{'delete':{'query':'targetPid:\"$pid\"'}}"
done

echo
for pid in $@; do
	echo "pid:\"$pid\" OR"
done

echo
for pid in $@; do
	echo "{'delete':{'query':'pid:\"$pid\"'}}"
done
