#!/bin/bash

for pid in $@; do
	curl -X 'GET' \
		"https://sdnnt.nkp.cz/sdnnt/api/v1.0/lists/info/$pid?digital_library=mzk" \
	    -H 'accept: application/json'
done
