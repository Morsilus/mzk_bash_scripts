#!/bin/bash

for rels in $@; do
	grep "\(has\|contains\)" $rels | grep -o "uuid:[0-9a-f-]*"
done
