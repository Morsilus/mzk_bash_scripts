#!/bin/bash

get_pids=".[{i}].model"
tmp="/tmp/has_items_in_order_`date +%s`.tmp"
tmp_dates="/tmp/has_items_in_order_`date +%s`_dates.tmp"
tmp_numbers="/tmp/has_items_in_order_`date +%s`_numbers.tmp"

for pid in $@; do
	model=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.model'`
	if [[ $model != "periodicalvolume" ]]; then
		echo "$pid,$pid,not_volume"
		break
	fi

	for item_pid in `curl -s $K5_HOST/api/v5.0/item/$pid/children | jq -r '.[].pid'`; do
		model=`curl -s $K5_HOST/api/v5.0/item/$item_pid | jq -r '.model'`
		if [[ $model == "page" ]]; then
			echo "$pid,$pid,pages_in_volume"
			break
		fi

		item_json=`curl -s $K5_HOST/api/v5.0/item/$item_pid`
		item_date=`echo $item_json | jq -r '.details.date' | sed 's/.*-//' | awk -F. '{print $3F" "$2F" "$1F" "}'`
		item_number=`echo $item_json | jq -r '.details.partNumber'`
		echo $item_date,$item_pid >> $tmp_dates
		echo $item_number,$item_pid >> $tmp_numbers
	done

	sort -nk1  -nk2 -nk3 $tmp_dates | cut -d, -f2 > $tmp; mv $tmp $tmp_dates
	sort -n $tmp_numbers | cut -d, -f2 > $tmp; mv $tmp $tmp_numbers
	#paste <(cat $tmp_dates) <(cat $tmp_numbers)

	response=`diff -q $tmp_dates $tmp_numbers`
	if [[ $response  == "" ]]; then
		echo $pid,True
	else
		echo $pid,False
	fi
	
	rm $tmp_dates
	rm $tmp_numbers
done
