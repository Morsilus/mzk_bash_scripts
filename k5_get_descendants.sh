#!/bin/bash

for pid in $@; do
	descendants=`curl -s "$K5_HOST/api/v5.0/search?q=root_pid:%22$pid%22&wt=json&fl=PID&rows=10000" | jq -r '.response.docs[].PID'`
	for descendant in $descendants; do
		echo $pid,$descendant
	done
done
