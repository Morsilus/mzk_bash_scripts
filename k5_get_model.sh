#!/bin/bash

for pid in $@; do
	model=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.model'`
	echo $pid,$model
done
