#!/bin/bash

nk_host="https://kramerius5.nkp.cz"

if [ $# -ne 1 ]; then
	exit 1
fi

status=`curl -s $nk_host/search/api/v5.0/item/$1 | jq '.status'`

if [[ $status == "404" ]]; then
	exit 1
fi
exit 0
