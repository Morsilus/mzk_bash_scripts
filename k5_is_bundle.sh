#!/bin/bash

for pid in $@; do
	models=`curl -s $K5_HOST/api/v5.0/item/$pid/children | jq -r '.[].model' | grep -v "monographunit"`
	if echo $models | grep -q "^\s*$"; then
		echo $pid,True
	else
		echo $pid,False
	fi
done
