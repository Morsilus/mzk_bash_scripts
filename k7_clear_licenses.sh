#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_num_planned="$K7_HOST/api/admin/v7.0/processes/batches?state=PLANNED&resultSize=1"
get_num_running="$K7_HOST/api/admin/v7.0/processes/batches?state=RUNNING&resultSize=1"
plan_process="$K7_HOST/api/admin/v7.0/processes"
log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

max_queue=15
wait_time=15
#scope="OBJECT"
scope="TREE"

processed_n=0
for pid in $@; do
	planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
	running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
	active=`echo "$planned+$running" | bc`
	while [[ $active -ge $max_queue ]]; do
		sleep $wait_time
		planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
		running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
		active=`echo "$planned+$running" | bc`
	done

	licenses=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=licenses" | jq -r '.response.docs[0].licenses'`
	
	if [[ $licenses != "null" ]]; then
		echo $licenses | jq -cr '.[]' | while read license; do
			body="'{\"defid\":\"remove_license\",\"params\":{\"license\":\"$license\",\"pid\":\"$pid\"}}'"
			cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_process\" | sed 's/$/\n/' >> $log_file"
			eval $cmd
		done
	fi
	
	processed_n=`echo "$processed_n+1" | bc`
	echo "$processed_n/$#"
done
