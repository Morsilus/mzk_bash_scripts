#!/bin/bash

for pid in $@; do
	scan="`curl -su $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/get/$pid/RELS-EXT | grep "http://imageserver.mzk.cz/" | cut -d\> -f2 | cut -d\< -f1`"
	echo $pid,$scan
done
