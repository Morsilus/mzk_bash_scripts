#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=physical_locations.facet" | jq -r '.response.docs[0]."physical_locations.facet"'`
done
