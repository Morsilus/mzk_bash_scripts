#!/bin/bash

auth="krameriusAdmin:krameriusAdmin"
header="\"Content-Type: application/json\""
planning_call="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?state=PLANNED&resultSize=1000"
running_call="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?state=RUNNING&resultSize=1000"

planned=`curl -s -u $auth -H "$header" $planning_call | jq length`
running=`curl -s -u $auth -H "$header" $running_call | jq length`
echo "$planned+$running" | bc
