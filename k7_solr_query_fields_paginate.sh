#!/bin/bash

start_index=0
fields="pid"
rows=1000

if [[ $# -eq 4 ]]; then
	start_index=$4
	fields=`echo $3 | sed 's/,/%2C/'`
	rows=$2
elif [[ $# -eq 3 ]]; then
	fields=`echo $3 | sed 's/,/%2C/'`
	rows=$2
elif [[ $# -eq 2 ]]; then
	rows=$2
elif [[ $# -ne 1 ]]; then
	exit 1
fi

query=`echo $1 | jq -s -R -r @uri`
response=`curl -s "$K7_HOST/api/client/v7.0/search?q=$query&fl=$fields&rows=$rows&start=$start_index"`

echo $response | jq -r ".response.docs[] | map(if type == \"array\" then join(\"|\") else . end) | join(\",\")"
