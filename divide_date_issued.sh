#!/bin/bash

function get_output_file {
	if echo $1 | grep -q "^[0-9]\{4\}$"; then echo "right_year"
	elif echo $1 | grep -q "^\[[0-9]\{4\}\]$"; then echo "right_year_in_brackets"
	elif echo $1 | grep -q "^([0-9]\{4\})$"; then echo "right_year_in_round_brackets"
	elif echo $1 | grep -q "^[0-9]\{4\}\s\?-\s\?[0-9]\{4\}$"; then echo "right_range"
	elif echo $1 | grep -q "^\[[0-9]\{4\}-[0-9]\{4\}\]$"; then echo "right_range_in_brackets"
	elif echo $1 | grep -q "^\[\?c[0-9]\{4\}\]\?$"; then echo "copyright_year"
	elif echo $1 | grep -q "^[0-9]\{4\}-$"; then echo "without_end_year"
	elif echo $1 | grep -q "^\[[0-9]\{4\}\]-$"; then echo "without_end_year_brackets"
	elif echo $1 | grep -q "^\[\?[0-9]\{4\}?\]\?$"; then echo "with_questionmark"
	elif echo $1 | grep -q "^\[\?[0-9]\{3\}-?\]\?$"; then echo "with_questionmark_with_last_year_unknown"
	elif echo $1 | grep -q "^\[\?[0-9]\{2\}--?\]\?$"; then echo "with_questionmark_with_decade_unknown"
	elif echo $1 | grep -q "^\[\?[0-9]\{3\}-\]\?$"; then echo "last_year_unknown"
	elif echo $1 | grep -q "^\[\?[0-9]\{2\}--\]\?$"; then echo "decade_unknown"
	elif echo $1 | grep -q "^\[[0-9]\{4\}$"; then echo "missmatched_brackets"
	elif echo $1 | grep -q "^\[[0-9]\{4\}-[0-9]\{4\}$"; then echo "missmatched_brackets_range"
	elif echo $1 | grep -q "^[0-9]\{4\}\]$"; then echo "missmatched_brackets"
	elif echo $1 | grep -q "^[0-9]\{4\}-[0-9]\{4\}\]$"; then echo "missmatched_brackets_range"
	elif echo $1 | grep -q "s\.d\."; then echo "sd"
	elif echo $1 | grep -q "s\. d\."; then echo "sd"
	elif echo $1 | grep -q "mezi"; then echo "mezi"
	elif echo $1 | grep -q "nebo"; then echo "nebo"
	elif echo $1 | grep -q "asi"; then echo "asi"
	elif echo $1 | grep -q "[0-9]\{4\},\?\s\?\[i\.e\.\s\?[0-9]\{4\}\]"; then echo "ie"
	elif echo $1 | grep -q "s\."; then echo "wrong_field"
	elif echo $1 | grep -q "[0-9]\{1,2\}\s*-\s*[0-9]\{1,2\}\."; then echo "missing_dots"
	elif echo $1 | grep -q "c[0-9]\{4\}[,-]\s\?c[0-9]\{4\}"; then echo "double_copyright"
	elif echo $1 | grep -q "tisk"; then echo "tisk"
	elif echo $1 | grep -q ".*;.*"; then echo "semicolon"
	elif echo $1 | grep -q "xx\.\([0-9]\{2\}\.\)\?[0-9]\{4\}"; then echo "xx"
	elif echo $1 | grep -q "\[po\s\?r\.\s\?[0-9]\{4\}?\?\]"; then echo "po_roku"
	elif echo $1 | grep -q "[0-9]\{4\},\?.*v\stir.*[0-9]\{4\}\]\?"; then echo "v_tir"
	elif echo $1 | grep -q "[0-9]\{4\},\?\s\[spr\.\s[0-9]\{4\}\]"; then echo "spr"
	elif echo $1 | grep -q "\[\?[0-9]\{4\},\?.*na\stit.*[0-9]\{4\}\]\?"; then echo "na_tit"
	elif echo $1 | grep -q "[0-9]\{4\}\s\?,\s\?\[\?c[0-9]\{4\}\]\?"; then echo "with_copyright_year"
	elif echo $1 | grep -q "^\[\?[0-9]\{4\}?\?\]\?\s\?,"; then echo "with_note"
	elif echo $1 | grep -q "^\[\?[0-9]\{4\}-[0-9]\{4\}?\?\]\?\s\?,"; then echo "with_note"
	elif echo $1 | grep -q "^[0-9]\{4\}\s\[.*\]$"; then echo "with_note_without_comma"
	elif echo $1 | grep -q "^[0-9]\{4\}[a-zA-Z\s]*$"; then echo "with_note_without_comma"
	elif echo $1 | grep -q "^[0-9]\{4\}[a-zA-Z\[\]\(\)\s]*$"; then echo "with_note_without_comma"
	else echo "unknown"
	fi
}

for xml in $@; do
	if ! [ -f $xml ]; then exit 1; fi
	declare -a dates=("`xmllint $xml --xpath '(//*[local-name()="dateIssued"])[1]/text()'`")
	len=${#dates[0]}
	if [ $len -gt 0 ]; then
		output=`get_output_file "${dates[0]}"`
		echo $xml\;${dates[0]}\;$output
	else
		declare -a dates=("`xmllint $xml --xpath '(//*[local-name()="date"])[1]/text()'`")
		len=${#dates[0]}
		if [ $len -gt 0 ]; then
			output=`get_output_file "${dates[0]}"`
			echo $xml\;${dates[0]}\;$output
		else
			echo $xml\;none\;none
		fi
	fi
done
