#!/bin/bash

tmp_rels="/tmp/needs_reindex_`date +%s`_rels.tmp"

for pid in $@; do
	curl -su $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/get/$pid/RELS-EXT > $tmp_rels
	children_json=`curl -s $K5_HOST/api/v5.0/item/$pid/children`
	n_json=`echo $children_json | jq length`
	n_rels=`cat $tmp_rels | grep ".*has.*uuid.*" | wc -l`
	if [[ $n_json -lt $n_rels ]]; then
		echo $pid
	fi
done

rm $tmp_rels