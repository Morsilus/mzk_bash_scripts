#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "Usage: $0 <records_file>"
	exit 1
fi

while IFS=, read -r pid date; do
	if [[ $date =~ ^[0-9]{4}$ ]]; then
		curl -s "$K7_HOST/api/client/v7.0/search?q=own_parent.pid:%22$pid%22%20AND%20date_range_start.year%3A$date%20AND%20date_range_end.year%3A$date&fl=pid" | jq -r '.response.docs[].pid'
	elif [[ $date =~ ^[0-9]{4}-(\*|[0-9]{4})$ ]]; then
		IFS=- read -r range_start range_end <<< "$date"
		curl -s "$K7_HOST/api/client/v7.0/search?q=own_parent.pid:%22$pid%22%20AND%20date_range_start.year%3A%5B$range_start%20TO%20$range_end%5D%20AND%20date_range_end.year%3A%5B$range_start%20TO%20$range_end%5D&fl=pid&rows=200" | jq -r '.response.docs[].pid'
	fi
done < "$1"

