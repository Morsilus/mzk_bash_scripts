#!/bin/bash

for uuid in $@; do
	curl $K7_HOST/api/client/v7.0/search?q=pid:%22$uuid%22 | jq '.response.docs[0]' > $uuid\_solr_k7.json
done
