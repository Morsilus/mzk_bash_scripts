#!/bin/bash

date=`date +%s`
tmp="tmp_$date"
echo $@ | sed 's/-/ /g' | sed 's/[^0-9 ]*//g' | tr ' ' '\n' > $tmp
num_sorted=`grep "[0-9]\+" $tmp | sort -n | tr ' ' '\n'`
rm $tmp
len=`echo "$num_sorted" | wc -l`
first=`echo "$num_sorted" | head -n 1`
last=`echo "$num_sorted" | tail -n 1`
num_generated=`seq $first $last`
echo ${num_sorted[@]} ${num_generated[@]} | tr ' ' '\n' | sort | uniq -u
