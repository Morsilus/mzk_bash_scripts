#!/bin/bash

auth="krameriusAdmin:krameriusAdmin"
header="\"Content-Type: application/json\""
api_call="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?def=reindex"

for pid in $@; do
	body="{\"parameters\":[\"reindexDoc\",\"$pid\"]}"
	curl -u $auth -H "$header" -d "'$body'" -X POST $api_call
done
