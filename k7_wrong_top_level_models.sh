#!/bin/bash

if [[ $# -gt 0 ]]; then
	exit 1
fi

query="level:0 AND -model:(monograph OR graphic OR map OR periodical OR sheetmusic OR soundrecording OR archive OR collection OR manuscript OR convolute)"
rows=50000

query=`echo $query | jq -s -R -r @uri`

response=`curl -s "$K7_HOST/api/client/v7.0/search?q=$query&fl=pid&rows=$rows"`
echo $response | jq -r '.response.docs[].pid'
