#!/bin/bash

for pid in $@; do
	curl -s $K7_HOST/api/client/v7.0/items/$pid/info/structure | jq
done
