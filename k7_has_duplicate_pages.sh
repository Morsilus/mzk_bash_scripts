#!/bin/bash

fields="page.number"

for pid in $@; do
	query=`echo "own_parent.pid:\"$pid\" AND model:\"page\"" | jq -s -R -r @uri`
	response=`curl -s "$K7_HOST/api/client/v7.0/search?q=$query&fl=$fields&rows=5000"`
	all_num=`echo $response | jq -r '.response.docs | length'`
	dupl_num=`echo $response | jq -r '.response.docs[]."page.number"' | sort | uniq | wc -l`
	if [[ $all_num -eq 0 ]]; then
		echo $pid,no_pages
	else
		echo $pid,$(((all_num - dupl_num) / dupl_num * 100))%
	fi
done

