#!/bin/bash

for pid in $@; do
	parent=`curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$pid%22&fl=root_pid&wt=json" | jq -r '.response.docs[0].root_pid'`
	echo $pid,$parent
done
