#!/bin/bash

for pid in $@; do
	policy=`curl -s $K5_HOST/api/v5.0/item/$pid | jq -r '.policy'`
	echo $pid,$policy
done
