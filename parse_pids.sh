#!/bin/bash

for line in $@; do
	if echo $line | grep -q "\([0-9a-f]\{8\}-\([0-9a-f]\{4\}-\)\{3\}[0-9a-f]\{12\}\)"; then
		echo $line | sed 's/.*\([0-9a-f]\{8\}-\([0-9a-f]\{4\}-\)\{3\}[0-9a-f]\{12\}\).*/uuid:\1/g'
	fi
done
