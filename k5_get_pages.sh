#!/bin/bash

for pid in $@; do
	descendants=`curl -s "$K5_HOST/api/v5.0/search?q=parent_pid:%22$pid%22%20AND%20fedora.model:page&wt=json&fl=PID&rows=10000" | jq -r '.response.docs[].PID'`
	for descendant in $descendants; do
		echo $pid,$descendant
	done
done
