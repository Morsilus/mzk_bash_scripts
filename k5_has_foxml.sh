#!/bin/bash

for pid in $@; do
	foxml=`curl -su $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/objects/$pid/objectXML`
	if echo $foxml | grep -q "no path in db registry for"; then
		echo $pid,False
	else
		echo $pid,True
	fi
done
