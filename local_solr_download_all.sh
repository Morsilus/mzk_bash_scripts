#!/bin/bash

LOCAL_HOST="http://localhost:8983/solr/"
BATCH_SIZE=10000
QUERY="%2A%3A%2A"

if [[ $# -ne 1 ]]; then
	exit 1
fi
core="$1"

base_url="$LOCAL_HOST$core"

batch_count=$((`curl -s "$base_url/select?q=$QUERY&rows=0" | jq '.response.numFound'` / BATCH_SIZE))

for batch in `seq 0 $batch_count`; do
	offset=$((BATCH_SIZE * batch))

	response=`curl -s "$base_url/select?q=$QUERY&rows=$BATCH_SIZE&start=$offset"`
	objects=$(echo "$response" | jq -c '.response.docs[]')
	uuid=$(uuidgen)
	echo $objects > $uuid\_$core.json

	#while read -r object; do
	#done <<< "$objects"
done

