#!/bin/bash

year="1952"
xpath='//*[local-name()="name"][@type="personal"]/*[local-name()="namePart"][@type="date"]/text()'
xml="/tmp/author_died_`date +%s`.xml"
tmp="/tmp/author_died_`date +%s`.tmp"

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS > $xml
	xmllint --xpath $xpath $xml 2> /dev/null | sed 's/\s//g' > $tmp
	for date_str in $(cat $tmp); do
		date=`echo $date_str | sed 's/.*\([0-9]\{4\}\)$/\1/'`
		if echo $date | grep -q "^[0-9]\{4\}$" ; then
			if [[ $date -eq $year ]]; then
				echo $pid
				break
			fi
		fi
	done
	rm $xml $tmp
done
