#!/bin/bash

for uuid in $@; do
	curl "$K5_HOST/api/v5.0/search?q=PID:%22$uuid%22&wt=json" | jq '.response.docs[]' > $uuid\_solr_k5.json
done
