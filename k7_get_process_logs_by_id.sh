#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_process="$K7_HOST/api/admin/v7.0/processes/by_process_id/\${process_id}"
get_logs_out="$K7_HOST/api/admin/v7.0/processes/by_process_uuid/\${process_uuid}/logs/out"
get_logs_err="$K7_HOST/api/admin/v7.0/processes/by_process_uuid/\${process_uuid}/logs/err"

for process_id in $@; do
	call=`eval "echo \"$(echo $get_process)\""`
	process_json=`curl -s -H "$header" -H "$auth_header" $call`
 	process_uuid=`echo $process_json | jq -r '.process.uuid'`

	defid=`echo $process_json | jq -r '.process.defid'`
	id=`echo $process_json | jq -r '.process.id'`
	state=`echo $process_json | jq -r '.process.state'`

	call=`eval "echo \"$(echo $get_logs_out)\""`
	curl -s -H "$header" -H "$auth_header" $call > $process_id\_out.txt
	if [[ $state == "FAILED" || $state == "WARNING" ]]; then
		call=`eval "echo \"$(echo $get_logs_err)\""`
		curl -s -H "$header" -H "$auth_header" $call > $process_id\_err.txt
	fi

	started=`echo $process_json | jq -r '.process.started'`
	finished_process=`echo $process_json | jq -r '.process.finished'`
	finished_batch=`echo $process_json | jq -r '.batch.finished'`
	batch_token=`echo $process_json | jq -r '.batch.token'`
	pid=`echo $process_json | jq -r '.process.name' | sed 's/.*\(uuid:[a-f0-9-]*\).*/\1/p' | uniq`
	count_pages=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=count_page" | jq -r '.response.docs[0].count_page'`
	echo $defid,$state,$id,$started,$finished_process,$finished_batch,$pid,$count_pages >> processes.csv
done
