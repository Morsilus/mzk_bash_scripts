#!/bin/bash

SET_PUBLIC_MUO_SHEETMUSIC="physical_locations.facet:KME450 AND model:sheetmusic AND level:0 AND -licenses:\"mzk_public-muo\""
SET_PUBLIC_MUO_CONVOLUTE="physical_locations.facet:KME450 AND model:sheetmusic AND level:1 AND own_parent.model:convolute AND -licenses_of_ancestors:\"mzk_public-muo\""
SET_PUBLIC="-model:collection AND ((level:0 AND -model:monograph AND -model:periodical) OR (level:0 AND model:monograph AND count_page:*) OR (level:0 AND model:monograph AND ds.img_full.mime:\"application/pdf\") OR (level:1 AND model:periodicalvolume) OR (level:1 AND model:monographunit AND -own_parent.model:convolute)) AND (accessibility:public AND -licenses:* AND -contains_licenses:* AND -licenses_of_ancestors:*)"
SET_ONSITE="-model:collection AND -model:sheetmusic AND ((level:0 AND -model:monograph AND -model:periodical) OR (level:0 AND model:monograph AND count_page:*) OR (level:0 AND model:monograph AND ds.img_full.mime:\"application/pdf\") OR (level:1 AND model:periodicalvolume) OR (level:1 AND model:monographunit AND -own_parent.model:convolute)) AND (accessibility:private AND -licenses:* AND -contains_licenses:* AND -licenses_of_ancestors:*)"
SET_ONSITE_SHEETMUSIC="model:sheetmusic AND level:0 AND accessibility:private AND -licenses:* AND -contains_licenses:* AND -licenses_of_ancestors:* AND -physical_locations.facet:KME450"

EXCLUSIVITY_1="licenses:(\"public\" AND \"mzk_public-muo\")"
EXCLUSIVITY_2="licenses:(\"public\" AND \"mzk_public-contract\")"
EXCLUSIVITY_3="licenses:(\"public\" AND \"onsite\")"
EXCLUSIVITY_4="licenses:(\"public\" AND \"onsite-sheetmusic\")"
EXCLUSIVITY_5="licenses:(\"dnnto\" AND \"dnntt\")"
EXCLUSIVITY_6="licenses:(\"onsite\" AND \"mzk_public-contract\")"
EXCLUSIVITY_7="licenses:(\"onsite\" AND \"onsite-sheetmusic\")"
EXCLUSIVITY_8="licenses:(\"onsite\" AND \"mzk_public-muo\")"
EXCLUSIVITY_9="licenses:(\"onsite-sheetmusic\" AND \"mzk_public-contract\")"
EXCLUSIVITY_10="licenses:(\"onsite-sheetmusic\" AND \"mzk_public-muo\")"

echo $EXCLUSIVITY_1
query=`echo $EXCLUSIVITY_1 | jq -s -R -r @uri`
curl -s "$K7_HOST/api/client/v7.0/search?q=$query&facet=true&facet.field=model&rows=0" | jq '.facet_counts.facet_fields.model' # | group_by(2)' # | map({key: .[0], value: .[1]})' # | to_entries' # | map(select(.value > 0)) | from_entries'

