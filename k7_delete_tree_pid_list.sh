#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
plan_process="$K7_HOST/api/admin/v7.0/processes"
log_file="/tmp/plan_gradually.log"

args=("$@")
pid_list=$(printf '%s\n' "${args[@]}" | jq -R . | jq -s .)
echo "$pid_list"

body="'{\"defid\":\"delete_tree\",\"params\":{\"pid\":\"$pid\"}}'"
cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_process\" | sed 's/$/\n/' >> $log_file"

echo $cmd
#eval $cmd
