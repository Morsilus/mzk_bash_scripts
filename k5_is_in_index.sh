#!/bin/bash

for pid in $@; do
	numFound=`curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$pid%22&fl=PID&rows=0&wt=json" | jq '.response.numFound'`
	if [[ $numFound -ge 1 ]]; then
		echo $pid,True
	else
		echo $pid,False
	fi
done
