#!/bin/bash

for pid in $@; do
	response=`curl -s "$K7_HOST/api/client/v7.0/search?q=root.pid%3A%22$pid%22&rows=0"`
	echo $pid,`echo $response | jq -r '.response.numFound'`
done
