for uuid in $(cat volumes); uuid=$uuid mods=$(cat $uuid\_mods.xml) dc=$(cat $uuid\_dc.xml) pages=$(cat $uuid\
_has_pages) envsubst < vol.xml > tmp; set name $(echo $uuid | cut -d: -f2 | sed 's/$/\.xml/'); xmllint --format tmp > $name; end
