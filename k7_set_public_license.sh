#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_num_planned="$K7_HOST/api/admin/v7.0/processes/batches?state=PLANNED&resultSize=1"
get_num_running="$K7_HOST/api/admin/v7.0/processes/batches?state=RUNNING&resultSize=1"
plan_process="$K7_HOST/api/admin/v7.0/processes"
log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

get_docs="https://k7-api.k7-test.mzk.cz/search/api/client/v7.0/search?q=accessibility:public%20AND%20-licenses:*%20AND%20level:0&fl=pid&rows=1000"
docs_file="/tmp/muo_pids"
max_queue=5
wait_time=15
scope="TREE"
license="public"

curl -s $get_docs | jq -r '.response.docs[].pid' > $docs_file
batch_size=`wc -l $docs_file | cut -d' ' -f1`
echo $batch_size
batch=1

while [[ $batch_size -ne 0 ]]; do
	processed_n=0
	for pid in `cat $docs_file`; do
		planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
		running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
		active=`echo "$planned+$running" | bc`
		while [[ $active -ge $max_queue ]]; do
			sleep $wait_time
			planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
			running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
			active=`echo "$planned+$running" | bc`
		done

		body="'{\"defid\":\"add_license\",\"params\":{\"license\":\"$license\",\"pid\":\"$pid\"}}'"
		cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_process\" | sed 's/$/\n/' >> $log_file"
		eval $cmd
		
		processed_n=`echo "$processed_n+1" | bc`
		echo "batch $batch: $processed_n/$batch_size"
	done

	batch=`echo "$batch+1" | bc`

	planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
	running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
	active=`echo "$planned+$running" | bc`
	while [[ $active -ne 0 ]]; do
		sleep $wait_time
		planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
		running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
		active=`echo "$planned+$running" | bc`
	done

	curl -s $get_docs | jq -r '.response.docs[].pid' > $docs_file
	batch_size=`wc -l $docs_file | cut -d' ' -f1`
	echo $batch_size
	sleep 180
done
