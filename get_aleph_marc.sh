#!/bin/bash

if [ $# -ne 2 ]; then
	exit 1
fi

url="http://aleph.mzk.cz/OAI?verb=GetRecord&identifier=oai:aleph.mzk.cz:${1^^}-$2&metadataPrefix=marc21"
curl $url > ${1^^}_$2.xml
