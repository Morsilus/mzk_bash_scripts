#!/bin/bash

for pid in $@; do
	numFound=`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=pid&rows=0" | jq '.response.numFound'`
	if [[ $numFound -ge 1 ]]; then
		echo $pid,True
	else
		echo $pid,False
	fi
done
