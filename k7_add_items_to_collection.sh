#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_num_planned="$K7_HOST/api/admin/v7.0/processes/batches?state=PLANNED&limit=1"
get_num_running="$K7_HOST/api/admin/v7.0/processes/batches?state=RUNNING&limit=1"

max_queue=15
wait_time=15

if [[ $# -ne 2 ]]; then
	exit 1
fi
if ! [[ -f $2 ]]; then
	exit 1
fi


size=`wc -l $2 | grep -o "^[0-9]*"`
processed_n=0
for pid in `cat $2`; do
	planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
	running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
	active=`echo "$planned+$running" | bc`
	while [[ $active -ge $max_queue ]]; do
		sleep $wait_time
		planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
		running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
		active=`echo "$planned+$running" | bc`
	done

	curl -s -H 'accept: application/json' -H "Authorization: Bearer $K7_TOKEN" "$K7_HOST/api/admin/v7.0/collections/$1/items" -X POST -H "Content-Type: text/plain" -d "$pid" | jq

	processed_n=$((processed_n+1))
	echo "$processed_n/$size"
done
