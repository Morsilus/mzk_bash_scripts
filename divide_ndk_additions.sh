#!/bin/bash

tmp_years="/tmp/divide_ndk_additions_`date +%s`_years.tmp"
result_dir="results"

extension="tsv"
year_row=3
result_rows="1,2,3,4,5,7,11,12,14,15"

if ! [[ -d $result_dir ]]; then
	mkdir $result_dir
fi

file="$1"
if echo "$file" | grep -vq "\.$extension$"; then
	exit 1
fi
cut -f$year_row "$file" | sort | uniq > $tmp_years
for year in `grep "^[0-9]\{4\}$" $tmp_years`; do
	awk -F'\t' "\$$year_row==$year" "$file" | cut -f$result_rows > $result_dir/$year.$extension
done


rm $tmp_years
