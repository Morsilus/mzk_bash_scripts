#!/bin/bash

for pid in $@; do
	status_code=`curl -s -u "$FEDORA_USER:$FEDORA_PASSWD" -o /dev/null -w "%{http_code}" "$FEDORA_HOST/objects/$pid/objectXML"`
	if [[ status_code -eq 200 ]]; then
		echo $pid,True
	else
		echo $pid,False
	fi
done
