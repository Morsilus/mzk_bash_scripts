#!/bin/bash

query=`echo $1 | jq -s -R -r @uri`
response=`curl -s "$K7_HOST/api/client/v7.0/search?q=$query&fl=pid&rows=0"`
echo $response | jq -r '.response.numFound'
