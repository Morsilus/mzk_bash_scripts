#!/bin/bash

get_child_model=".[{i}].model"
get_item_number=".[{i}].details.partNumber"
#get_item_number=".[{i}].details.issueNumber"
tmp_mods="/tmp/is_item_missing_`date +%s`_mods.xml"

if [[ $# -ne 2 ]]; then
	exit 2
fi

curl -s $K5_HOST/api/v5.0/item/$1/streams/BIBLIO_MODS > $tmp_mods
desc=`xmllint --xpath '//*[local-name()="physicalDescription"]/*/text()' $tmp_mods 2> /dev/null`
rm $tmp_mods

if echo $desc | grep -q "[a-zA-Z\. ]$2 "; then
	exit 1
fi

exit 0
