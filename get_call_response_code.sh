#!/bin/bash

for url in $@; do
	code=`curl -o /dev/null -s -w "%{http_code}\n" $url`
	echo $url,$code
done
