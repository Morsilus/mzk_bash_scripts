#!/bin/bash

for uuid in $@; do
	item=`curl -s $K5_HOST/api/v5.0/item/$uuid`
	is_pdf=`echo $item | jq '.pdf'`
	if [[ $is_pdf == "null" ]]; then
		echo $uuid,false
	else
		echo $uuid,true
	fi
done
