#!/bin/bash

for pid in $@; do
	level=`curl -s "$K5_HOST/api/v5.0/search?q=PID:%22$pid%22&wt=json&fl=level" | jq '.response.docs[0].level'`
	echo $pid,$level
done
