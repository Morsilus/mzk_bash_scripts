#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s $K7_HOST/api/client/v7.0/items/$pid/info/structure | jq '.children.own | length'`
done
