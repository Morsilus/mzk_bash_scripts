#/bin/bash

tmp="/tmp/foxml_add_img_streams.tmp"

if [[ $# -ne 2 ]]; then
	exit 1
fi
if ! [[ -f $1 ]]; then
	exit 1
fi

prev_content=`head -n-1 $1`
created=`xmllint --xpath 'string(//*[local-name()="property"][@NAME="info:fedora/fedora-system:def/model#createdDate"]/@VALUE)' $1`
url=$2

echo "$prev_content
	<foxml:datastream ID=\"IMG_FULL\" STATE=\"A\" CONTROL_GROUP=\"E\" VERSIONABLE=\"false\">
		<foxml:datastreamVersion ID=\"IMG_FULL.0\" LABEL=\"\" CREATED=\"$created\" MIMETYPE=\"image/jpeg\">
		<foxml:contentLocation TYPE=\"URL\" REF=\"$url/big.jpg\"/>
		</foxml:datastreamVersion>
	</foxml:datastream>
	<foxml:datastream ID=\"IMG_PREVIEW\" STATE=\"A\" CONTROL_GROUP=\"E\" VERSIONABLE=\"false\">
		<foxml:datastreamVersion ID=\"IMG_PREVIEW.0\" LABEL=\"\" CREATED=\"$created\" MIMETYPE=\"image/jpeg\">
		<foxml:contentLocation TYPE=\"URL\" REF=\"$url/preview.jpg\"/>
		</foxml:datastreamVersion>
	</foxml:datastream>
	<foxml:datastream ID=\"IMG_THUMB\" STATE=\"A\" CONTROL_GROUP=\"E\" VERSIONABLE=\"false\">
		<foxml:datastreamVersion ID=\"IMG_THUMB.0\" LABEL=\"\" CREATED=\"$created\" MIMETYPE=\"image/jpeg\">
		<foxml:contentLocation TYPE=\"URL\" REF=\"$url/thumb.jpg\"/>
		</foxml:datastreamVersion>
	</foxml:datastream>
	<foxml:datastream ID=\"IMG_FULL_ADM\" STATE=\"A\" CONTROL_GROUP=\"X\" VERSIONABLE=\"false\">
		<foxml:datastreamVersion ID=\"IMG_FULL_ADM.0\" LABEL=\"Image administrative metadata\" CREATED=\"$created\" MIMETYPE=\"text/xml\" SIZE=\"108\">
		<foxml:xmlContent>
		<adm:Description xmlns:adm=\"http://www.qbizm.cz/kramerius-fedora/image-adm-description\"/>
		</foxml:xmlContent>
		</foxml:datastreamVersion>
	</foxml:datastream>
</foxml:digitalObject>" > $tmp

xmllint --format $tmp > $1
rm $tmp
