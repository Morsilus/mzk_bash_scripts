#!/bin/bash

nk_host="https://kramerius5.nkp.cz/search"

for pid in $@; do
	licenses=`curl -s $nk_host/api/v5.0/item/$pid | jq -r '."dnnt-labels"[]' 2> /dev/null`
	echo $pid,$licenses
done
