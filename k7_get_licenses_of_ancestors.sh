#!/bin/bash

for pid in $@; do
	echo $pid,`curl -s "$K7_HOST/api/client/v7.0/search?q=pid:%22$pid%22&fl=licenses_of_ancestors" | jq -r '.response.docs[0].licenses_of_ancestors[0]'`
done
