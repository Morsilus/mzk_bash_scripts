#!/bin/bash

if [[ $# -eq 2 ]]; then
	rows=$2
elif [[ $# -eq 1 ]]; then
	rows=1000
else
	exit 1
fi

query=`echo $1 | jq -s -R -r @uri`

response=`curl -s "$K7_DEV_HOST/api/client/v7.0/search?q=$query&fl=pid&rows=$rows"`
echo $response | jq -r '.response.docs[].pid'
