#!/bin/bash

if [ $# -ne 1 ]; then
	exit 1
fi
if ! [ -f $1 ]; then
	exit 1
fi

signatures=`unzip -l $1 | tr -s ' ' | cut -d' ' -f 5 | grep ".*\.\(DSA\|RSA\|SF\)\$"`
for file in $signatures; do
	zip -d $1 $file
done
