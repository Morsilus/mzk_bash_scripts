#!/bin/bash

auth="krameriusAdmin:krameriusAdmin"
header="\"Content-Type: application/json\""
def="parametrizedimport"
get_imports="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?def=$def&resultSize={size}&offset={offset}"

size=100

offset=0
while [[ $offset -lt $1 ]]; do
	api_call=`echo $get_imports | sed "s/{size}/$size/" | sed "s/{offset}/$offset/"`
	curl -s -u $auth -H $header $api_call | jq 

	offset=`echo "$offset+$size" | bc`
done
