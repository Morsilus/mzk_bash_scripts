#!/bin/bash

year="1952"

xpath_date='//*[local-name()="name"][@type="personal"]/*[local-name()="namePart"][@type="date"]/text()'
xpath_name='//*[local-name()="name"][@type="personal"]/*[local-name()="namePart"][not(@*)]/text()'

xml="/tmp/author_name_died_`date +%s`.xml"
tmp_date="/tmp/author_name_died_`date +%s`_date.tmp"
tmp_name="/tmp/author_name_died_`date +%s`_name.tmp"

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS > $xml
	xmllint --xpath $xpath_date $xml 2> /dev/null | sed 's/\s//g' > $tmp_date
	xmllint --xpath $xpath_name $xml 2> /dev/null | sed 's/\s//g' > $tmp_name
	i="1"
	for date_str in $(cat $tmp_date); do
		date=`echo $date_str | sed 's/.*\([0-9]\{4\}\)$/\1/'`
		if echo $date | grep -q "^[0-9]\{4\}$" ; then
			if [[ $date -eq $year ]]; then
				sed -n "$i p" $tmp_name
			fi
		fi
		i=`echo "$i+1" | bc`
	done
	rm $xml $tmp_date $tmp_name
done
