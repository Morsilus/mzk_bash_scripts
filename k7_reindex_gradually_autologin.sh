#!/bin/bash

auth_header="Authorization: Bearer $K7_TOKEN"
header="Content-Type: application/json"
get_authenticated="$KC_HOST/realms/kramerius/protocol/openid-connect/userinfo"

get_num_planned="$K7_HOST/api/admin/v7.0/processes/batches?state=PLANNED&resultSize=1"
get_num_running="$K7_HOST/api/admin/v7.0/processes/batches?state=RUNNING&resultSize=1"
plan_reindex="$K7_HOST/api/admin/v7.0/processes"

log_file="/tmp/plan_gradually.log"
data_file="/tmp/plan_gradually_data_`date +%s`.json"

max_queue=7
wait_time=10
#reindex_type="OBJECT"
reindex_type="TREE_AND_FOSTER_TREES"

function reindex_pid {
	pid=$1
	planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
	running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
	active=`echo "$planned+$running" | bc`
	while [[ $active -ge $max_queue ]]; do
		sleep $wait_time
		planned=`curl -s -H "$auth_header" "$get_num_planned" | jq '.total_size'`
		running=`curl -s -H "$auth_header" "$get_num_running" | jq '.total_size'`
		active=`echo "$planned+$running" | bc`
	done

	body="'{\"defid\":\"new_indexer_index_object\",\"params\":{\"type\":\"$reindex_type\",\"pid\":\"$pid\"}}'"
	cmd="curl -s -H \"$header\" -H \"$auth_header\" -X POST -d $body \"$plan_reindex\" | sed 's/$/\n/' >> $log_file"
	eval $cmd
}

processed_n=0
for pid in $@; do
	#reindex_pid $pid

	if ! `curl -s -H "$auth_header" "$get_authenticated" | jq 'has("email")'`; then
		export K7_TOKEN=`python $K7_TOKEN_SCRIPT $KC_HOST $K7_SECRET $K7_USER $K7_PASSWD`
		echo $K7_TOKEN
	fi

	processed_n=`echo "$processed_n+1" | bc`
	echo "$processed_n/$#"
done
