#!/bin/bash

if [[ $# -eq 2 ]]; then
	rows=$2
elif [[ $# -eq 1 ]]; then
	rows=1000
else
	exit 1
fi

query=`echo $1 | jq -s -R -r @uri`
response=`curl -s "$SOLR_HOST/select?q=$query&fl=PID&rows=0&wt=json"`
echo $response | jq -r '.response.numFound'
