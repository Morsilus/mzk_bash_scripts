#!/bin/bash

if [[ $# -ne 1 ]]; then
	exit 1
fi

md_tmp="/tmp/output.md"
html_tmp="/tmp/output.html"
file=$1

pandoc -s "$file" -o $md_tmp
pandoc $md_tmp -o $html_tmp

html-beautify $html_tmp | sed 's/<\/\?mark>//g'

rm $md_tmp
rm $html_tmp
