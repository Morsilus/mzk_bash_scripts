#!/bin/bash

curl "http://aleph.mzk.cz/OAI?verb=GetRecord&identifier=oai:aleph.mzk.cz:$1&metadataPrefix=marc21" > marc21_tmp
xmllint --format marc21_tmp > $1.xml
