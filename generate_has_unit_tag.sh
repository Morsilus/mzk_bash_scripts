#!/bin/bash

xml="/tmp/generate_has_unit.xml"
tmp="/tmp/generate_has_unit.tmp"

rm $tmp &> /dev/null

for pid in $@; do
	curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS > $xml
	part_number=`xmllint --xpath '//*[local-name()="partNumber"]/text()' $xml`
	date=`xmllint --xpath '//*[local-name()="dateIssued"]/text()' $xml`
	rm $xml
	echo "$part_number,$date,<hasUnit xmlns=\"http://www.nsdl.org/ontologies/relationships#\" rdf:resource=\"info:fedora/$pid\"></hasUnit>" >> $tmp
done

sort -n $tmp | cut -d, -f3

rm $tmp
