#!/bin/bash

k5_tmp="/tmp/k5_mods.xml"
k7_tmp="/tmp/k7_mods.xml"

xpath='//*[local-name()="note"]/text()'

for pid in $@; do
	k5_mods=`curl -s $K5_HOST/api/v5.0/item/$pid/streams/BIBLIO_MODS`
	k7_mods=`curl -s $K7_HOST/api/client/v7.0/items/$pid/metadata/mods`

	if ! echo $k5_mods | grep -q modsCollection ; then
		echo $pid,not_found_in_k5
		continue
	fi
	if ! echo $k7_mods | grep -q modsCollection ; then
		echo $pid,not_found_in_k7
		continue
	fi

	echo $k5_mods > $k5_tmp
	echo $k7_mods > $k7_tmp

	k5_element=`xmllint --xpath $xpath $k5_tmp`
	k7_element=`xmllint --xpath $xpath $k7_tmp`

	if [[ $k5_element == $k7_element ]]; then
		echo $pid,same
	else
		echo $pid,different
	fi
done

if [[ -f $k5_tmp ]]; then
	rm $k5_tmp $k7_tmp
fi
