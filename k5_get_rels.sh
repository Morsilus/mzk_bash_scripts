#!/bin/bash

for pid in $@; do
	curl -u $FEDORA_USER:$FEDORA_PASSWD $FEDORA_HOST/get/$pid/RELS-EXT > $pid\_rels.xml
done
