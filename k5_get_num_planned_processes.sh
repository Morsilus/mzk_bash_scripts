#!/bin/bash

auth="krameriusAdmin:krameriusAdmin"
header="\"Content-Type: application/json\""
api_call="http://kramerius.infra.mzk.cz/search/api/v4.6/processes?state=PLANNED&resultSize=1000"

curl -s -u $auth -H "$header" $api_call | jq length
