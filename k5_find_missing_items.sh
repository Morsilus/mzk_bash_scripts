#!/bin/bash

get_child_model=".[{i}].model"
get_item_number=".[{i}].details.partNumber"
#get_item_number=".[{i}].details.issueNumber"
tmp_item_numbers="/tmp/find_missing_items_`date +%s`_items.tmp"
tmp_all_numbers="/tmp/find_missing_items_`date +%s`_all.tmp"

missing=""

function missing_items_for_volume {
	missing=""
	children_json=`curl -s $K5_HOST/api/v5.0/item/$1/children`
	length=`echo $children_json | jq length`
	length=`echo "$length-1" | bc`

	has_some=0
	for i in `seq 0 $length`; do
		query=`echo $get_child_model | sed "s/{i}/$i/"`
		model=`echo $children_json | jq $query | sed 's/\"//g'`
		if [[ $model = "periodicalitem" ]]; then
			query=`echo $get_item_number | sed "s/{i}/$i/"`
			number=`echo $children_json | jq $query | sed 's/\"//g'`
			if echo $number | grep -q "^[0-9\s]*-[0-9\s]*$"; then
				first=`echo $number | sed 's/-[0-9]*$//'`
				last=`echo $number | sed 's/^[0-9]*-//'`
				for i in `seq $first $last`; do
					echo $i >> $tmp_item_numbers
				done
				has_some=1
			elif echo $number | grep -q "^[0-9]*$"; then
				echo $number >> $tmp_item_numbers
				has_some=1
			fi
		fi
	done

	if [[ has_some -eq 0 ]]; then
		return
	fi

	sort -no $tmp_item_numbers $tmp_item_numbers
	first=`head -n 1 $tmp_item_numbers`
	last=`tail -n 1 $tmp_item_numbers`
	for i in `seq $first $last`; do
		echo $i >> $tmp_all_numbers
	done

	sort -o $tmp_item_numbers $tmp_item_numbers
	sort -o $tmp_all_numbers $tmp_all_numbers

	missing=`comm -13 $tmp_item_numbers $tmp_all_numbers | sort -n`

	rm $tmp_item_numbers
	rm $tmp_all_numbers
}

for pid in $@; do
    missing_items_for_volume $pid
	if [[ $missing != "" ]]; then
		echo $pid,$missing
	fi
done